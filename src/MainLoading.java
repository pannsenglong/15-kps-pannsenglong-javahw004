import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainLoading
{
    public static void main(String args[])
    {
        Store<Integer> store = new Store<>();
        store.setItems(new Integer[0]);
        store.addItem(1);
        store.addItem(null);
        store.addItem(1);
        store.addItem(3);
        store.addItem(9);

        System.out.println("\nList of array is below: ");
        for (int i=0; i<store.size() ; i++ )
        {
            System.out.println("Index[" + i + "] = " + store.getItem(i));
        }
    }
}
