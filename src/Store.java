
import java.util.Arrays;

public class Store<T>
{
    private T items[];
    private int index = 0;

    public T[] getItems() {
        return items;
    }

    public void setItems(T[] items) {
        this.items = items;
    }

    //add element to array
    public void addItem(T value)
    {
        //create more size of array
        if (index >= items.length) {
            int resize = items.length + 1;
            items = Arrays.copyOf(items, resize);
        }

        try {
            //null exception
            if (value == null) {
                throw new NumberFormatException("Inputted Null Value");
            }

            //duplicate exception
            for (int i=0; i<items.length; i++)
            {
                if(value == items[i])
                {
                    throw new Exception("Duplicate Element");
                }
            }
            //add acceptable value to array
            this.items[index++] = value;
        }
        catch (Exception ex)
        {
            System.out.println(ex);
        }

        //clear last null value of array
        if(items[items.length-1] == null)
        {
            items = Arrays.copyOf(items, items.length-1);
        }
    }

    //get element from array
    public T getItem(int index)
    {
       T itemGet = items[index];
       return itemGet;
    }


    // to get the last size of array
    public int size()
    {
        return items.length;
    }
}
